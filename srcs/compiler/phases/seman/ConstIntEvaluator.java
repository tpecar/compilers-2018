package compiler.phases.seman;

import common.report.*;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;

/**
 * A visitor that computes the value of a constant integer expression.
 * 
 * The visitor is extended from the AbsNullVisitor in order to get default
 * visit methods for all possible class instances that are not relevant for
 * this visitor.
 * 
 * The purpose of this visitor is not to modify the AST, but to evalate the
 * possible expression, in the cases where this is expected - when we have
 * a type system rule that expects a integer constant. In the cases when the
 * ConstIntEvaluator evaluates to null, this will generally infer a type system
 * error.
 * 
 * Also, the reason we try to shy away from AST modification is that, besides
 * being messy, it throws of file positions - the evaluated value does not
 * exist in the original file.
 * 
 * @author sliva
 *
 */
public class ConstIntEvaluator extends AbsNullVisitor<Long, Object> {
    @Override
    public Long visit(AbsAtomExpr atomExpr, Object visArg) {
        if(atomExpr.type == AbsAtomExpr.Type.INT)
            try {
                return new Long(atomExpr.expr);
            }
            catch(NumberFormatException e) {}
        return null;
    }
    @Override
    public Long visit(AbsBinExpr binExpr, Object visArg) {
        // evaluate operands, and if we are able to retrieve a value from both,
        // we evaluate their parent expression
        Long l, r;
        if ((l = binExpr.fstExpr.accept(this, null)) != null &&
            (r = binExpr.sndExpr.accept(this, null)) != null)
            switch(binExpr.oper) {
                case ADD: return l + r;
                case SUB: return l - r;
                case MUL: return l * r;
                case DIV: 
                    if(r == 0) throw new Report.Error(binExpr, "Division by 0");
                    return l / r;
                case MOD:
                    if(r == 0) throw new Report.Error(binExpr, "Division by 0");
                    return l % r;
            }
        // in all other cases, the expression was not evaluated
        return null;
    }
    @Override
    public Long visit(AbsUnExpr unExpr, Object visArg) {
        Long op = unExpr.subExpr.accept(this, null);
        if(op != null)
            switch(unExpr.oper) {
                case ADD: return op;
                case SUB: return -op;
            }
        return null;
    }
}
