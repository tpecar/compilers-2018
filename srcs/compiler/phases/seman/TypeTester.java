package compiler.phases.seman;

import common.report.*;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;
import static compiler.phases.seman.SemAn.*;

/**
 * Tests whether types constructed by {@link TypeDefiner} make sense.
 * 
 * This is where we check for mutually recursive type definitions.
 * 
 * Essentially, after we create a SemNamedType in TypeDefiner, we use this
 * visitor to check whether following the type declaration chain leads to some
 * final type or goes too deep.
 * 
 * We will assume that a type declaration chain of length NONREC_MAX or over is
 * recursive.
 * 
 * @author sliva
 *
 */
public class TypeTester extends AbsNullVisitor<Object, Object> {
    /**
     * Maximum length of type declaration chain.
     */
    private final int NONREC_MAX = 10;
    
    /**
     * Indicator that the type is recursive.
     */
    public static final Object TYPE_RECURSIVE = new Object();
    
    /**
     * Checks if the type is recursive.
     * 
     * @param typeName The type we are following.
     * @param visArg The current position in declaration chain.
     * @return TYPE_RECURSIVE if recursive, otherwise null
     */
    @Override
    public Object visit(AbsTypeName typeName, Object visArg) {
        if((Integer)visArg >= NONREC_MAX)
            return TYPE_RECURSIVE;
        
        AbsDecl decl = BIND(typeName);
        // follow only if the type is declared
        if(decl instanceof AbsTypeDecl)
            // at this point we know we will visit another type declaration,
            // advance chain length and descend
            return ((AbsTypeDecl) decl).type.accept(this, (Integer)visArg + 1);
        return null;
    }
}
