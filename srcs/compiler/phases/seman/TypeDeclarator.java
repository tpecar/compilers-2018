package compiler.phases.seman;

import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;

/**
 * Declares type synonyms introduced by type declarations.
 * 
 * Methods of this visitor return {@code null} but leave their results in
 * {@link SemAn#declType()}.
 * 
 * Here we are creating TYPED bindings.
 * 
 * This visitor does not actually behave as a visitor, but is essentially a
 * container for nodes that generate same bindings.
 * NameChecker forwards all functionality related to TYPED binding to it.
 * 
 * Therefore we are using pure AbsVisitor here, since any visit not handled here
 * is essentially an error that we want to catch.
 * 
 * @author sliva
 *
 */
public class TypeDeclarator implements AbsVisitor<Object, Object> {
    /*
    438   if      :   n>=0
    438               [[decl 1 ]]TYPED = T1 , . . . , [[decl n ]]TYPED = Tn
    438               [[type]]ISTYPE = T
    438               T1 , . . . , Tn , T -> {void, bool, char, int} U {ptr(T ) | T -> Td }
    438   valid   :   [[IDENTIFIER(decl 1 , . . . , decl n ):type]]TYPED = (T1 , . . . , Tn ) -> T
    */
    @Override
    public Object visit(AbsFunDef funDef, Object visArg) {
        // TYPED
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return null;
    }
    /*
    425   if      :   n >= 0
    425               [[expr 1 ]]OFTYPE = T1 , . . . , [[expr n ]]OFTYPE = Tn
    425               [[IDENTIFIER]]BIND = fun decl
    425               [[decl ]]TYPED = (T1 , . . . , Tn ) -> T
    425   valid   :   [[IDENTIFIER(expr 1 , . . . , expr n )]]OFTYPE = T
    */
    @Override
    public Object visit(AbsFunName funName, Object visArg) {
        funName.args.accept(this, visArg);
        return null;
    }
}
