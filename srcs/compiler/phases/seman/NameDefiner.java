package compiler.phases.seman;

import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;

/**
 * A visitor that traverses (a part of) the AST and stores all declarations
 * encountered into the symbol table. It is meant to be called from another
 * visitor, namely {@link NameChecker}.
 * 
 * @author sliva
 *
 */
public class NameDefiner extends AbsFullVisitor<Object, Object> {

    /** The symbol table. */
    private final SymbTable symbTable;
    private final NameChecker nameChecker;
    
    /**
     * A status object that is provided as visArg in the first declaration
     * pass.
     * 
     * This is required for the two pass function declaration processing,
     * which is required in the case when two functions on the same scope
     * call each other.
     * 
     * The object itself does not carry any info, we will use its reference
     * number.
     */
    public static final Object PREDECL = new Object();

    /**
     * Constructs a new name checker using the specified symbol table.
     * 
     * @param nameChecker
     *            Name checker that checks currently present names.
     *            (this is a slight hack)
     * @param symbTable
     *            The symbol table, to which we add newly discovered names.
     */
    public NameDefiner(NameChecker nameChecker, SymbTable symbTable) {
        this.nameChecker = nameChecker;
        this.symbTable = symbTable;
    }
    
    @Override
    public Object visit(AbsDecls decls, Object visArg) {
        for (AbsDecl decl : decls.decls())
            // first pass only processes function names, second pass will
            // process function parameters & bodies
            if(visArg == PREDECL) {
                if (decl instanceof AbsFunDecl || decl instanceof AbsFunDef)
                    decl.accept(this, visArg);
            }
            else
                decl.accept(this, null);
        return null;
    }
    
    // function declarations, while they define no functionality, give us the
    // ability to hook external code at linker stage, so we have to process them
    @Override
    public Object visit(AbsFunDecl funDecl, Object visArg) {
        // function name is still in the old scope (visible outside the function
        // itself)
        if(visArg == PREDECL) {
            symbTable.ins(funDecl);
            return null;
        }
        funDecl.type.accept(nameChecker, null); // check return type of fun. declaration
        // function declaration has no body, but scope checks still have to be
        // done in order to dissalow duplicate arguments
        symbTable.newScope();
        funDecl.parDecls.accept(this, null);
        symbTable.oldScope();
        return null;
    }

    @Override
    public Object visit(AbsFunDef funDef, Object visArg) {
        if(visArg == PREDECL) {
            symbTable.ins(funDef);
            return null;
        }
        funDef.type.accept(nameChecker, null); // check return type of fun. definition
        // function body, have to create new scope for arguments
        symbTable.newScope();
        funDef.parDecls.accept(this, null);
        
        // visit function body
        funDef.value.accept(nameChecker, null);
        // old scope outside function
        symbTable.oldScope();
        return null;
    }
    
    @Override
    public Object visit(AbsParDecl parDecl, Object visArg) {
        parDecl.type.accept(nameChecker, visArg); // check the type of function parameter
        symbTable.ins(parDecl);
        return null;
    }
    
    @Override
    public Object visit(AbsRecType recType, Object visArg) {
        // We descend with a new NameDefiner instance that has
        //  - the current nameChecker instance, which allows us to check if
        //    the record component uses a valid type, defined in upper scope
        //  - new symbTable, which represents the record scope, that is
        //    independent from the scope of the compound statement
        
        SymbTable recSymbTable = new SymbTable();
        recType.compDecls.accept(new NameDefiner(nameChecker, recSymbTable), null);
        
        // bind the record symbol table to its node
        SemAn.recSymbTable().put(recType, recSymbTable);
        
        return null;
    }
    
    @Override
    public Object visit(AbsTypeDecl typeDecl, Object visArg) {
        typeDecl.type.accept(nameChecker, null); // check the type used in type declaration
        // assumed that types are in the same namespace as identifiers
        symbTable.ins(typeDecl);
        return null;
    }
    
    @Override
    public Object visit(AbsVarDecl varDecl, Object visArg) {
        varDecl.type.accept(nameChecker, null); // check the type of variable
        symbTable.ins(varDecl);
        return null;
    }
}
