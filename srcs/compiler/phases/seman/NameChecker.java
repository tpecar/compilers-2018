package compiler.phases.seman;

import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;

/**
 * A visitor that traverses (a part of) the AST and checks if all names used are
 * visible where they are used. This visitor uses another visitor, namely
 * {@link NameDefiner}, whenever a declaration is encountered during the AST
 * traversal.
 * 
 * Since we are mostly just traversing the AST, we are using the AbsFullVisitor
 * as the basis.
 *
 * @author sliva
 *
 */
public class NameChecker extends AbsFullVisitor<Object, Object> {

    // TODO: two-step function handling!!
    // see Slivnik's test-03.prev for the example that causes problem
    
    /**
     * The symbol table.
     */
    private final SymbTable symbTable;
    
    /**
     * NameDefiner instance that uses the NameCheckers symbol table.
     */
    private final NameDefiner nameDefiner;

    /**
     * Constructs a new name checker using the specified symbol table.
     *
     * @param symbTable The symbol table.
     */
    public NameChecker(SymbTable symbTable) {
        this.symbTable = symbTable;
        
        nameDefiner = new NameDefiner(this, symbTable);
    }

    @Override
    public Object visit(AbsFunName funName, Object visArg) {
        // check if this function call refers to a valid function
        symbTable.check(funName);
        return null;
    }

    // _LBRACE stmt stmtX _COLON expr whereExpr _RBRACE
    // Since a compound expression is required for declaration of variables,
    // this should be the only entry point in NameChecker that calls NameDefiner
    @Override
    public Object visit(AbsStmtExpr stmtExpr, Object visArg) {
        // create a new scope & visit the declaration section
        symbTable.newScope();
        stmtExpr.decls.accept(nameDefiner, NameDefiner.PREDECL);
        stmtExpr.decls.accept(nameDefiner, null);
        // visit statements & expression
        stmtExpr.stmts.accept(this, null);
        stmtExpr.expr.accept(this, null);
        
        // return to old scope (outside compound expression)
        symbTable.oldScope();
        return null;
    }

    @Override
    public Object visit(AbsTypeName typeName, Object visArg) {
        symbTable.check(typeName);
        return null;
    }

    @Override
    public Object visit(AbsVarName varName, Object visArg) {
        symbTable.check(varName);
        return null;
    }

}
