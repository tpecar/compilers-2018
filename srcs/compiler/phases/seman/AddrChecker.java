package compiler.phases.seman;

import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import static compiler.phases.seman.SemAn.*;

/**
 * Checks if a given node could be used as an LValue.
 * This can later be checked through the SemAn.isLValue.
 * If the node is not present in SemAn.isLValue, we assume
 * that it is not an Lvalue.
 * 
 * The visitor itself disregards the argument and returns null.
 * @author tpecar
 */
public class AddrChecker extends AbsFullVisitor<Object, Object> {

    @Override
    public Object visit(AbsArrExpr arrExpr, Object visArg) {
        // descend & check if lvalues
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        
        // valid    [[ expr [expr'] ]]LVAL = true
        // if       [[ expr ]]LVAL = true
        if(LVAL(arrExpr.array))
            tagLVAL(arrExpr);
        return null;
    }

    @Override
    public Object visit(AbsRecExpr recExpr, Object visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        
        // valid    [[ expr.IDENTIFIER ]]LVAL = true
        // if       [[ expr ]]LVAL = true
        if(LVAL(recExpr.record))
            tagLVAL(recExpr);
        return null;
    }

    @Override
    public Object visit(AbsUnExpr unExpr, Object visArg) {
        unExpr.subExpr.accept(this, visArg);
        
        // valid    [[ @ expr ]]LVAL = true
        // if       [[expr ]]LVAL = true
        if(unExpr.oper == AbsUnExpr.Oper.VAL &&
            LVAL(unExpr.subExpr))
            tagLVAL(unExpr);
        return null;
    }

    @Override
    public Object visit(AbsVarName varName, Object visArg) {
        // valid    [[ IDENTIFIER ]]BIND E { (variable declaration), (parameter declaration) }
        // if       [[ IDENTIFIER ]]LVAL = true
        AbsDecl decl = BIND(varName);
        if(decl instanceof AbsVarDecl || decl instanceof AbsParDecl)
            tagLVAL(varName);
        return null;
    }
}
