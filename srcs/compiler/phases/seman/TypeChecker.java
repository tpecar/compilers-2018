package compiler.phases.seman;

import common.report.*;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;

/**
 * Tests whether expressions are well typed.
 * 
 * Methods of this visitor return the semantic type of a phrase being tested if
 * the AST node represents an expression or {@code null} otherwise. In the first
 * case methods leave their results in {@link SemAn#isOfType()}.
 * 
 * See TypeRules.txt for full rule list.
 * 
 * Here we are either creating OFTYPE binding or invoking the TypeDeclarator,
 * TypeDefiner visitor to create other types of bindings.
 * 
 * @author sliva
 *
 */
public class TypeChecker extends AbsFullVisitor<SemType, Object> {
    /*
    426   if      :   [[expr 1 ]]OFTYPE = arr(n × T )
    426               [[expr 2 ]]OFTYPE = int
    426   valid   :   [[expr 1 [expr 2 ]]]OFTYPE = T
    */
    @Override
    public SemType visit(AbsArrExpr arrExpr, Object visArg) {
        arrExpr.array.accept(this, visArg);
        arrExpr.index.accept(this, visArg);
        return null;
    }
    /*
    405   if      :   [[expr ]]VAL = n
    405               [[type]]ISTYPE = T
    405   valid   :   [[arr[expr ]type]]ISTYPE = arr(n × T )
    */
    @Override
    public SemType visit(AbsArrType arrType, Object visArg) {
        // ISTYPE
        arrType.len.accept(this, visArg);
        arrType.elemType.accept(this, visArg);
        return null;
    }
    /*
    432   if      :   [[expr 1 ]]OFTYPE = T
    432               [[expr 2 ]]OFTYPE = T
    432               [[expr 1 ]]LVAL = true
    432   valid   :   [[expr 1 = expr 2 ]]OFTYPE = void
    */
    @Override
    public SemType visit(AbsAssignStmt assignStmt, Object visArg) {
        assignStmt.dst.accept(this, visArg);
        assignStmt.src.accept(this, visArg);
        return null;
    }
    /*
    409   if      :   -/-
    409   valid   :   [[none]]OFTYPE = void

    410   if      :   -/-
    410   valid   :   [[null]]OFTYPE = ptr(void)

    411   if      :   -/-
    411   valid   :   [[BOOLEAN]]OFTYPE = bool

    412   if      :   -/-
    412   valid   :   [[CHAR]]OFTYPE = char

    413   if      :   -/-
    413   valid   :   [[INTEGER]]OFTYPE = int
    */
    @Override
    public SemType visit(AbsAtomExpr atomExpr, Object visArg) {
        return null;
    }
    /*
    401   if      :   -/-
    401   valid   :   [[void]]ISTYPE = void

    402   if      :   -/-
    402   valid   :   [[bool]]ISTYPE = bool

    403   if      :   -/-
    403   valid   :   [[char]]ISTYPE = char

    404   if      :   -/-
    404   valid   :   [[int]]ISTYPE = int
    */
    @Override
    public SemType visit(AbsAtomType atomType, Object visArg) {
        // ISTYPE
        return null;
    }
    /*
    415   if      :   [[expr ]]OFTYPE = int
    415               op -> {+, -}
    415   valid   :   [[op expr]]OFTYPE = int

    416   if      :   [[expr 1 ]]OFTYPE = bool
    416               [[expr 2 ]]OFTYPE = bool
    416               op -> {|, ^, &}
    416   valid   :   [[expr 1 op expr 2 ]]OFTYPE = bool

    417   if      :   [[expr 1 ]]OFTYPE = int
    417               [[expr 2 ]]OFTYPE = int
    417               op -> {+, -, *, /, %}
    417   valid   :   [[expr 1 op expr 2 ]]OFTYPE = int

    418   if      :   [[expr 1 ]]OFTYPE = T
    418               [[expr 2 ]]OFTYPE = T
    418               op -> {==, !=, <, >, <=, >=}
    418               T -> {bool, char, int} U {ptr(T ) | T -> Td }
    418   valid   :   [[expr 1 op expr 2 ]]OFTYPE = bool
    */
    @Override
    public SemType visit(AbsBinExpr binExpr, Object visArg) {
        binExpr.fstExpr.accept(this, visArg);
        binExpr.sndExpr.accept(this, visArg);
        return null;
    }
    /*
    423   if      :   [[type]]ISTYPE = T1
    423               [[expr ]]OFTYPE = T2
    423               <T1 , T2> i -> {<void, T> | T -> Td } U
    423               {<int, int>, <int, char>, <int, bool>} U
    423               {<ptr(T ), ptr(void)> | T -> Td }
    423   valid   :   [[[type]expr ]]OFTYPE = T1
    */
    @Override
    public SemType visit(AbsCastExpr castExpr, Object visArg) {
        castExpr.type.accept(this, visArg);
        castExpr.expr.accept(this, visArg);
        return null;
    }
    /*
    422   if      :   [[expr ]]OFTYPE = ptr(T )
    422               T != void
    422   valid   :   [[del expr ]]OFTYPE = void
    */
    @Override
    public SemType visit(AbsDelExpr delExpr, Object visArg) {
        delExpr.expr.accept(this, visArg);
        return null;
    }
    /*
    438   if      :   n>=0
    438               [[decl 1 ]]TYPED = T1 , . . . , [[decl n ]]TYPED = Tn
    438               [[type]]ISTYPE = T
    438               T1 , . . . , Tn , T -> {void, bool, char, int} U {ptr(T ) | T -> Td }
    438   valid   :   [[IDENTIFIER(decl 1 , . . . , decl n ):type]]TYPED = (T1 , . . . , Tn ) -> T
    */
    @Override
    public SemType visit(AbsFunDecl funDecl, Object visArg) {
        // TYPED
        // evaluate subnode types first
        funDecl.parDecls.accept(this, visArg);
        funDecl.type.accept(this, visArg);
        return null;
    }
    /*
    438   if      :   n>=0
    438               [[decl 1 ]]TYPED = T1 , . . . , [[decl n ]]TYPED = Tn
    438               [[type]]ISTYPE = T
    438               T1 , . . . , Tn , T -> {void, bool, char, int} U {ptr(T ) | T -> Td }
    438   valid   :   [[IDENTIFIER(decl 1 , . . . , decl n ):type]]TYPED = (T1 , . . . , Tn ) -> T
    */
    @Override
    public SemType visit(AbsFunDef funDef, Object visArg) {
        // TYPED
        funDef.parDecls.accept(this, visArg);
        funDef.type.accept(this, visArg);
        funDef.value.accept(this, visArg);
        return null;
    }
    /*
    425   if      :   n >= 0
    425               [[expr 1 ]]OFTYPE = T1 , . . . , [[expr n ]]OFTYPE = Tn
    425               [[IDENTIFIER]]BIND = fun decl
    425               [[decl ]]TYPED = (T1 , . . . , Tn ) -> T
    425   valid   :   [[IDENTIFIER(expr 1 , . . . , expr n )]]OFTYPE = T
    */
    @Override
    public SemType visit(AbsFunName funName, Object visArg) {
        funName.args.accept(this, visArg);
        return null;
    }
    /*
    433   if      :   [[expr ]]OFTYPE = bool
    433               [[stmts]]OFTYPE = void
    433   valid   :   [[if expr then stmts end]]OFTYPE = void

    434   if      :   [[expr ]]OFTYPE = bool
    434               [[stmts 1 ]]OFTYPE = void
    434               [[stmts 2 ]]OFTYPE = void
    434   valid   :   [[if expr then stmts 1 else stmts 2 end]]OFTYPE = void
    */
    @Override
    public SemType visit(AbsIfStmt ifStmt, Object visArg) {
        ifStmt.cond.accept(this, visArg);
        ifStmt.thenBody.accept(this, visArg);
        ifStmt.elseBody.accept(this, visArg);
        return null;
    }
    /*
    421   if      :   [[type]]ISTYPE = T
    421               T != void
    421   valid   :   [[new type]]OFTYPE = ptr(T )
    */
    @Override
    public SemType visit(AbsNewExpr newExpr, Object visArg) {
        newExpr.type.accept(this, visArg);
        return null;
    }
    /*
    406   if      :   [[type]]ISTYPE = T
    406   valid   :   [[ptr type]]ISTYPE = ptr(T )
    */
    @Override
    public SemType visit(AbsPtrType ptrType, Object visArg) {
        // ISTYPE
        ptrType.subType.accept(this, visArg);
        return null;
    }
    /*
    427   if      :   [[expr ]]OFTYPE = rec(T1 , . . . , Tn )
    427               [[IDENTIFIER]]BIND = decl
    427               [[decl ]]TYPED = T
    427   valid   :   [[expr.IDENTIFIER]]OFTYPE = T
    */
    @Override
    public SemType visit(AbsRecExpr recExpr, Object visArg) {
        recExpr.record.accept(this, visArg);
        recExpr.comp.accept(this, visArg);
        return null;
    }
    /*
    407   if      :   n > 0
    407               [[type 1 ]]ISTYPE = T1 , . . . , [[type n ]]ISTYPE = Tn
    407   valid   :   [[rec{ IDENTIFER1 :type 1 , . . . , IDENTIFERn :type n }]]ISTYPE = rec(T1 , . . . , Tn )
    */
    @Override
    public SemType visit(AbsRecType recType, Object visArg) {
        // ISTYPE
        recType.compDecls.accept(this, visArg);
        return null;
    }
    /*
    428   if      :   [[stmts]]OFTYPE = void
    428               [[expr ]]OFTYPE = T
    428   valid   :   [[{stmts:expr }]]OFTYPE = T

    429   if      :   [[stmts]]OFTYPE = void
    429               [[expr ]]OFTYPE = T
    429   valid   :   [[{stmts:expr where decls}]]OFTYPE = T
    */
    @Override
    public SemType visit(AbsStmtExpr stmtExpr, Object visArg) {
        stmtExpr.decls.accept(this, visArg);
        stmtExpr.stmts.accept(this, visArg);
        stmtExpr.expr.accept(this, visArg);
        return null;
    }
    /*
    436   if      :   [[stmt 1 ]]OFTYPE = void, . . . , [[stmt n ]]OFTYPE = void
    436   valid   :   [[stmt 1 ; . . . ;stmt n ]]OFTYPE = void
    */
    @Override
    public SemType visit(AbsStmts stmts, Object visArg) {
        for (AbsStmt stmt : stmts.stmts()) {
            stmt.accept(this, visArg);
        }
        return null;
    }
    /*
    437   if      :   [[type]]ISTYPE = T
    437   valid   :   [[IDENTIFIER:type]]TYPED = T
    */
    @Override
    public SemType visit(AbsTypeDecl typeDecl, Object visArg) {
        // TYPED
        typeDecl.type.accept(this, visArg);
        return null;
    }
    /*
    408   if      :   [[IDENTIFIER]]BIND = typ decl
    408               [[decl ]]TYPED = T
    408   valid   :   [[IDENTIFIER]]ISTYPE = T
    */
    @Override
    public SemType visit(AbsTypeName typeName, Object visArg) {
        // ISTYPE
        return null;
    }
    /*
    414   if      :   [[expr ]]OFTYPE = bool
    414   valid   :   [[! expr ]]OFTYPE = bool

    415   if      :   [[expr ]]OFTYPE = int
    415               op -> {+, -}
    415   valid   :   [[op expr]]OFTYPE = int

    419   if      :   [[expr ]]OFTYPE = T
    419               T != void
    419               [[expr ]]LVAL = true
    419   valid   :   [[$ expr ]]OFTYPE = ptr(T )

    420   if      :   [[expr ]]OFTYPE = ptr(T )
    420               T != void
    420   valid   :   [[@ expr ]]OFTYPE = T
    */
    @Override
    public SemType visit(AbsUnExpr unExpr, Object visArg) {
        unExpr.subExpr.accept(this, visArg);
        return null;
    }
    // nothing to do here - the variable usage uses the 
    // declAt binding ([[. ]]BIND) to get to its declaration
    /*
    @Override
    public SemType visit(AbsVarDecl varDecl, Object visArg) {
        varDecl.type.accept(this, visArg);
        return null;
    }
    */
    
    /*
    424   if      :   [[IDENTIFIER]]BIND = var decl
    424               [[decl ]]TYPED = T
    424   valid   :   [[IDENTIFIER]]OFTYPE = T
    */
    @Override
    public SemType visit(AbsVarName varName, Object visArg) {
        return null;
    }
    /*
    435   if      :   [[expr ]]OFTYPE = bool
    435               [[stmts]]OFTYPE = void
    435   valid   :   [[while expr do stmts end]]OFTYPE = void
    */
    @Override
    public SemType visit(AbsWhileStmt whileStmt, Object visArg) {
        whileStmt.cond.accept(this, visArg);
        whileStmt.body.accept(this, visArg);
        return null;
    }
}
