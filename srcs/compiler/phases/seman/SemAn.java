package compiler.phases.seman;

import common.report.Report;
import compiler.phases.*;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;

/**
 * Semantic analysis.
 *
 * @author sliva
 *
 */
public class SemAn extends Phase {
    
        /**
         * If enabled, provides detailed internal debugging.
         * The debugging information should be printed out with Report.info,
         * format is implementation dependent.
         */
        public static boolean DEBUG = false;

	/** The attribute that maps the usage of a name to its declaration.
         * 
         *  Provides the BIND mapping.
         */
	private static final AbsAttribute<AbsName, AbsDecl> declAt = new AbsAttribute<AbsName, AbsDecl>();
        public static AbsDecl BIND(AbsName n) {
            AbsDecl d = declAt.get(n);
            if(d == null)
                throw new Report.InternalError(String.format("No BIND for %s", n));
            return d;
        }
        public static void BIND(AbsName n, AbsDecl d) {
            declAt.put(n, d);
        }
        
	/**
	 * The attribute that maps maps a type declaration to an internal
	 * representation of a declared type.
         * 
         * Provides the TYPED mapping.
         * 
         * Later type retrievals can be achieved using
         * declType.get(declAt.get(type_name))
	 */
	private static final AbsAttribute<AbsTypeDecl, SemNamedType> declType = new AbsAttribute<AbsTypeDecl, SemNamedType>();
        public static SemNamedType TYPED(AbsTypeDecl d) {
            SemNamedType t = declType.get(d);
            if(t == null)
                throw new Report.InternalError(String.format("No TYPED for %s @ %s", d.name, d.location));
            return t;
        }
        public static void TYPED(AbsTypeDecl d, SemNamedType t) {
            declType.put(d, t);
        }
        
	/**
	 * The attribute that maps a type expression to an internal representation
	 * of a described type.
         * 
         * Provides the ISTYPE mapping.
	 */
	private static final AbsAttribute<AbsType, SemType> descType = new AbsAttribute<AbsType, SemType>();
        public static SemType ISTYPE(AbsType t) {
            SemType s = descType.get(t);
            if(s == null)
                throw new Report.InternalError(String.format("No ISTYPE for %s", t));
            return s;
        }
        public static void ISTYPE(AbsType t, SemType s) {
            descType.put(t, s);
        }
        
        
	/**
	 * The attribute that maps an expression to an internal representation of
	 * its type.
         * 
         * Provides the OFTYPE mapping.
	 */
	private static final AbsAttribute<AbsExpr, SemType> isOfType = new AbsAttribute<AbsExpr, SemType>();
        public static SemType OFTYPE(AbsExpr e) {
            SemType t = isOfType.get(e);
            if(t == null)
                throw new InternalError(String.format("No OFTYPE for %s", e));
            return t;
        }
        public static void OFTYPE(AbsExpr e, SemType t) {
            isOfType.put(e, t);
        }
        
	/** The attribute that maps a record to its symbol table. */
	private static final AbsAttribute<AbsRecType, SymbTable> recSymbTable = new AbsAttribute<AbsRecType, SymbTable>();
        
	/**
	 * The attribute that tells whether an expression can evaluate to an lvalue.
         * 
         * Provides the LVAL mapping.
	 */
	private static final AbsAttribute<AbsExpr, Boolean> isLValue = new AbsAttribute<AbsExpr, Boolean>();
        // essentially, the isLValue will (in our case) contain only
        // true valued Boolean objects, so the presence of mapping means the
        // child node is an LValue
        //  - this is messy, but its how the template was set up
        public static boolean LVAL(AbsExpr e) {
            return isLValue.get(e) != null;
        }
        public static void tagLVAL(AbsExpr e) {
            isLValue.put(e, Boolean.TRUE);
        }

	/**
	 * Returns an attribute that maps the usage of a name to its declaration.
	 * 
	 * @return The attribute that maps the usage of a name to its declaration.
	 */
	public static AbsAttribute<AbsName, AbsDecl> declAt() {
		return declAt;
		// vrne declAt - lahko notri vstavljamo, ali ga sprasujemo ali je za kaksno ime deklarirano
		// ko gremo skozi drevo, prvic pogledamo kje so stvari definirane
		//    dodamo v simbolno tabelo
		//    potem ko najdemo uporabo, pogledamo v simbolno tabelo, dodamo mu atribut declAt
	}

	/**
	 * Returns an attribute that maps maps a type declaration to an internal
	 * representation of a declared type.
	 * 
	 * @return The attribute that maps maps a type declaration to an internal
	 *         representation of a declared type.
	 */
	public static AbsAttribute<AbsTypeDecl, SemNamedType> declType() {
		return declType;
		// zaenkrat ignoriramo - naslednji teden
	}

	/**
	 * Returns an attribute that maps a type expression to an internal
	 * representation of a described type.
	 * 
	 * @return The attribute that maps a type expression to an internal
	 *         representation of a described type.
	 */
	public static AbsAttribute<AbsType, SemType> descType() {
		return descType;
	}

	/**
	 * Returns an attribute that maps an expression to an internal
	 * representation of its type.
	 * 
	 * @return The attribute that maps an expression to an internal
	 *         representation of its type.
	 */
	public static AbsAttribute<AbsExpr, SemType> isOfType() {
		return isOfType;
	}

	/**
	 * Returns an attribute that maps a record to its symbol table.
	 * 
	 * @return The attribute that maps a record to its symbol table.
	 */
	public static AbsAttribute<AbsRecType, SymbTable> recSymbTable() {
		return recSymbTable;
	}

	/**
	 * Returns an attribute that tells whether an expression can evaluate to an
	 * lvalue.
	 * 
	 * @return The attribute that tells whether an expression can evaluate to an
	 *         lvalue.
	 */
	public static AbsAttribute<AbsExpr, Boolean> isLValue() {
		return isLValue;
	}

	/**
	 * Constructs a new semantic analysis phase.
	 */
	public SemAn() {
		super("seman");
	}

	@Override
	public void close() {
		declAt.lock();
		declType.lock();
		descType.lock();
		isOfType.lock();
		recSymbTable.lock();
		Abstr.absTree().accept(new AbsLogger(logger).addSubvisitor(new SemLogger(logger)), null);
		super.close();
	}

	// potrebno je se izracunati konstantne podizraze
	// char s[10+1];
}
