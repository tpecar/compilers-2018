package compiler.phases.seman;

import java.util.*;
import common.report.*;
import compiler.phases.abstr.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.seman.type.*;
import static compiler.phases.seman.SemAn.*;

/**
 * Constructs semantic representation of each type expression.
 * 
 * Methods of this visitor return the constructed semantic type if the AST node
 * represents a type or {@code null} otherwise. In either case methods leave
 * their results in {@link SemAn#descType()}.
 * 
 * Here we are creating ISTYPE bindings.
 * 
 * This visitor does not actually behave as a visitor, but is essentially a
 * container for nodes that generate same bindings.
 * NameChecker forwards all functionality related to ISTYPE binding to it.
 * 
 * Therefore we are using pure AbsVisitor here, since any visit not handled here
 * is essentially an error that we want to catch.
 * 
 * @author sliva
 *
 */
public class TypeDefiner implements AbsVisitor<SemType, Object> {
    /*
    405   if      :   [[expr ]]VAL = n
    405               [[type]]ISTYPE = T
    405   valid   :   [[arr[expr ]type]]ISTYPE = arr(n × T )
    */
    @Override
    public SemType visit(AbsArrType arrType, Object visArg) {
        // ISTYPE
        // [[expr ]]VAL = n
        Long n = arrType.len.accept(new ConstIntEvaluator(), null);
        if(n == null)
            throw new Report.Error(
                arrType,
                String.format("Could not evaluate len expression @ %s", arrType.len.location)
            );
        return null;
    }
    
    /*
    401   if      :   -/-
    401   valid   :   [[void]]ISTYPE = void

    402   if      :   -/-
    402   valid   :   [[bool]]ISTYPE = bool

    403   if      :   -/-
    403   valid   :   [[char]]ISTYPE = char

    404   if      :   -/-
    404   valid   :   [[int]]ISTYPE = int
    */
    @Override
    public SemType visit(AbsAtomType atomType, Object visArg) {
        // ISTYPE
        SemType T = null;
        // [[void]]ISTYPE = void
        if(atomType.type == AbsAtomType.Type.VOID)
            T = new SemVoidType();
        
        // [[bool]]ISTYPE = bool
        if(atomType.type == AbsAtomType.Type.BOOL)
            T = new SemBoolType();
        
        // [[char]]ISTYPE = char
        if(atomType.type == AbsAtomType.Type.CHAR)
            T = new SemCharType();
        
        // [[int]]ISTYPE = int
        if(atomType.type == AbsAtomType.Type.INT)
            T = new SemIntType();
        
        if(T == null)
            throw new Report.InternalError(
                String.format("Provided type %s is unhandled", atomType.type)
            );
        
        ISTYPE(atomType, T);
        return T;
    }
    
    /*
    406   if      :   [[type]]ISTYPE = T
    406   valid   :   [[ptr type]]ISTYPE = ptr(T )
    */
    @Override
    public SemType visit(AbsPtrType ptrType, Object visArg) {
        // ISTYPE
        // [[type]]ISTYPE = T
        SemType T = ISTYPE(ptrType.subType);
        
        // [[ptr type]]ISTYPE = ptr(T )
        SemPtrType ptr = new SemPtrType(T);
        ISTYPE(ptrType, ptr);
        return ptr;
    }
    
    /*
    407   if      :   n > 0
    407               [[type 1 ]]ISTYPE = T1 , . . . , [[type n ]]ISTYPE = Tn
    407   valid   :   [[rec{ IDENTIFER1 :type 1 , . . . , IDENTIFERn :type n }]]ISTYPE = rec(T1 , . . . , Tn )
    */
    @Override
    public SemType visit(AbsRecType recType, Object visArg) {
        // ISTYPE
        // n > 0
        Vector<AbsCompDecl> T = recType.compDecls.compDecls();
        if(T.isEmpty())
            throw new Report.Error(recType, "Record needs at least one component");
        
        // [[type 1 ]]ISTYPE = T1 , . . . , [[type n ]]ISTYPE = Tn
        Vector<String> compNames = new Vector<>();
        Vector<SemType> compTypes = new Vector<>();
        
        for(AbsCompDecl cAbsCompDecl : T) {
            compNames.add(cAbsCompDecl.name);
            // types of subcomponents were already defined in TypeChecker
            compTypes.add(ISTYPE(cAbsCompDecl.type));
        }
        
        // [[rec{ IDENTIFER1 :type 1 , . . . , IDENTIFERn :type n }]]ISTYPE = rec(T1 , . . . , Tn )
        SemRecType rec = new SemRecType(compNames, compTypes);
        ISTYPE(recType, rec);
        return rec;
    }
    
    /*
    408   if      :   [[IDENTIFIER]]BIND = typ decl
    408               [[decl ]]TYPED = T
    408   valid   :   [[IDENTIFIER]]ISTYPE = T
    */
    @Override
    public SemType visit(AbsTypeName typeName, Object visArg) {
        // ISTYPE
        // [[IDENTIFIER]]BIND = typ decl
        if(typeName.accept(new TypeTester(), 0) == TypeTester.TYPE_RECURSIVE)
            throw new Report.Error(typeName,
                String.format(
                    "%s has a type %s @ %s with recursive definition",
                    typeName.name, BIND(typeName).name, BIND(typeName).location
                    )
            );
        AbsDecl decl = BIND(typeName);
        
        // [[decl ]]TYPED = T
        if(!(decl instanceof AbsTypeDecl))
            throw new Report.Error(typeName,
                String.format(
                    "%s expected to be a type declaration, but instead it was %s:%s @ %s",
                    typeName.name,
                    decl.name, decl.type, decl.location
                )
            );
        SemNamedType T = TYPED((AbsTypeDecl)decl);
        
        // [[IDENTIFIER]]ISTYPE = T
        ISTYPE(typeName, T);
        return T;
    }
}
