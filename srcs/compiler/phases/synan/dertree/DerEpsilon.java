/*
 *  See about_license.txt in repo root.
 */
package compiler.phases.synan.dertree;

import common.report.Location;
import compiler.phases.lexan.Symbol;
import compiler.phases.synan.DerVisitor;

/**
 * Epsilon expansion in the derivation.
 * @author tpecar
 */
public class DerEpsilon extends DerTree {
    public final Location location;
    
    public DerEpsilon(Location location) {
        this.location = location;
    }
    public DerEpsilon(Symbol symb) {
        this.location = symb.location();
    }
    
    /**
     * Copy constructor.
     */
    public DerEpsilon(DerEpsilon de) {
        this.location = de.location;
    }
    
    @Override
    public Location location() {
        return location;
    }
    
    @Override
    public <Result, Arg> Result accept(DerVisitor<Result, Arg> visitor, Arg accArg) {
            return visitor.visit(this, accArg);
    }
}
