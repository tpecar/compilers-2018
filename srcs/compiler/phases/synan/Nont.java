package compiler.phases.synan;

/**
 * CFG nonterminals.
 * 
 * @author sliva
 *
 */
public enum Nont {
    Source,
    accessExpr,
    accessExprX,
    addExpr,
    addExprX,
    andExpr,
    andExprX,
    assignStmt,
    compExpr,
    compExprX,
    decl,
    declExtra,
    declFunParams,
    declFunParamsX,
    declRecParams,
    declRecParamsX,
    declX,
    elseStmt,
    expr,
    idFunExpr,
    idFunExprParams,
    idFunExprParamsX,
    mulExpr,
    mulExprX,
    orExpr,
    orExprX,
    stmt,
    stmtX,
    type,
    unaryExpr,
    valueExpr,
    whereExpr
}
