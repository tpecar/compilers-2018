(based on prev.pdf, 2016/17)

Source -> expr .

expr -> orExpr .

orExpr -> andExpr orExprX .

orExprX -> _IOR andExpr orExprX .
orExprX -> _XOR andExpr orExprX .
orExprX -> .

andExpr -> compExpr andExprX .

andExprX -> _AND compExpr andExprX .
andExprX -> .

compExpr -> addExpr compExprX .

compExprX -> _EQU addExpr .
compExprX -> _NEQ addExpr .
compExprX -> _LEQ addExpr .
compExprX -> _GEQ addExpr .
compExprX -> _LTH addExpr .
compExprX -> _GTH addExpr .
compExprX -> .

addExpr -> mulExpr addExprX .

addExprX -> _ADD mulExpr addExprX .
addExprX -> _SUB mulExpr addExprX .
addExprX -> .

mulExpr -> unaryExpr mulExprX .

mulExprX -> _MUL unaryExpr mulExprX .
mulExprX -> _DIV unaryExpr mulExprX .
mulExprX -> _MOD unaryExpr mulExprX .
mulExprX -> .

unaryExpr -> _ADD unaryExpr .
unaryExpr -> _SUB unaryExpr .
unaryExpr -> _NOT unaryExpr .
unaryExpr -> _MEM unaryExpr .
unaryExpr -> _NEW type .
unaryExpr -> _DEL unaryExpr .
unaryExpr -> _VAL unaryExpr .
unaryExpr -> _LBRACKET type _RBRACKET unaryExpr .
unaryExpr -> accessExpr .

accessExpr -> valueExpr accessExprX .

accessExprX -> _DOT _IDENTIFIER accessExprX.
accessExprX -> .
accessExprX -> _LBRACKET expr _RBRACKET accessExprX .

valueExpr -> _VOIDCONST .
valueExpr -> _BOOLCONST .
valueExpr -> _CHARCONST .
valueExpr -> _INTCONST .
valueExpr -> _PTRCONST .
valueExpr -> _IDENTIFIER idFunExpr .
valueExpr -> _LPARENTHESIS expr _RPARENTHESIS .
valueExpr -> _LBRACE stmt stmtX _COLON expr whereExpr _RBRACE .

idFunExpr -> _LPARENTHESIS idFunExprParams _RPARENTHESIS .
idFunExpr -> .

idFunExprParams -> expr idFunExprParamsX .
idFunExprParams -> .

idFunExprParamsX -> _COMMA expr idFunExprParamsX .
idFunExprParamsX -> .

stmtX -> _SEMIC stmt stmtX .
stmtX -> .

whereExpr -> _WHERE decl declX .
whereExpr -> .

declX -> _SEMIC decl declX .
declX -> .

stmt -> _IF expr _THEN stmt stmtX elseStmt _END .
stmt -> _WHILE expr _DO stmt stmtX _END .
stmt -> expr assignStmt .

assignStmt -> _ASSIGN expr .
assignStmt -> .

elseStmt -> _ELSE stmt stmtX.

type -> _VOID .
type -> _BOOL .
type -> _CHAR .
type -> _INT .
type -> _ARR _LBRACKET expr _RBRACKET type .
type -> _PTR type .
type -> _REC _LPARENTHESIS declRecParams _RPARENTHESIS .
type -> _IDENTIFIER .

declRecParams -> _IDENTIFIER _COLON type declRecParamsX .
declRecParams -> .

declRecParamsX -> _COMMA _IDENTIFIER _COLON type declRecParamsX .
declRecParamsX -> .

decl -> _TYP _IDENTIFIER _COLON type .
decl -> _VAR _IDENTIFIER _COLON type .
decl -> _FUN _IDENTIFIER _LPARENTHESIS declFunParams _RPARENTHESIS _COLON type declExtra .

declFunParams -> _IDENTIFIER _COLON type declFunParamsX .
declFunParams -> .

declFunParamsX -> _COMMA _IDENTIFIER _COLON type declFunParamsX .
declFunParamsX -> .

declExtra -> _ASSIGN expr .
declExtra -> .
