/*
 *  See about_license.txt in repo root.
 */
package compiler.phases.lexan.dfa;

import java.util.ArrayList;

/** An extension of the List interface, that allows for chained appends.
 * 
 *  The initial plan was:
 *      To utilize an implementation of the List interface in AppendableList,
 *      cast as ((AppendableList<E>)(List<E>)new ListImplementation<E>())
 *  And while the compiler happily consumes that, it will explode during runtime.
 */
public class AppendableArrayList<E> extends ArrayList<E> {
    public AppendableArrayList<E> append(E e) {
        this.add(e);
        return this;
    }
}
