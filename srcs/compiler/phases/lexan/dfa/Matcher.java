/*
 *  See about_license.txt in repo root.
 */
package compiler.phases.lexan.dfa;

import java.util.List;
import java.util.stream.Collectors;

/** Character matcher. */
public interface Matcher {
    /** Checks if the specified character matches.
     * @param in input char
     * @return if the character matched
     */
    public boolean match(char in);

    /** Matches specific character. */
    static class CharMatcher implements Matcher {
        private final char match;
        public CharMatcher(char match) {
            this.match = match;
        }
        @Override
        public boolean match(char in) {
            return this.match == in;
        }
        @Override
        public String toString() {
            return String.format("[%c]", match);
        }
    }
    /** Matches specified range, start/end limits included. */
    static class CharRangeMatcher implements Matcher {
        private final char rangeStart, rangeEnd;
        public CharRangeMatcher(char rangeStart, char rangeEnd) {
            if(rangeEnd < rangeStart)
                throw new IllegalArgumentException("rangeStart should be lower than rangeEnd");
            this.rangeStart = rangeStart;
            this.rangeEnd = rangeEnd;
        }
        @Override
        public boolean match(char in) {
            return in >= this.rangeStart && in <= this.rangeEnd;
        }
        @Override
        public String toString() {
            return String.format("[%c - %c]", rangeStart, rangeEnd);
        }
    }
    /** Matches inverse of the specified matcher. */
    static class InverseMatcher implements Matcher {
        private final Matcher m;
        public InverseMatcher(Matcher m) {
            this.m = m;
        }
        @Override
        public boolean match(char in) {
            return !m.match(in);
        }
        @Override
        public String toString() {
            return String.format("!(%s)", m.toString());
        }
    }
    /** Matches an union of matchers. */
    static class UnionMatcher implements Matcher {
        private final List<Matcher> m;
        public UnionMatcher(List<Matcher> m) {
            this.m = m;
        }
        @Override
        public boolean match(char in) {
            boolean match = false;
            for(Matcher cm : m)
                if(match = cm.match(in))
                    break;
            return match;
        }
        @Override
        public String toString() {
            return m.stream().map(Matcher::toString).collect(Collectors.joining(" U "));
        }
    }

    // typical ranges
    static final Matcher
            az      = new CharRangeMatcher('a', 'z'),
            AZ      = new CharRangeMatcher('A', 'Z'),
            _09     = new CharRangeMatcher('0', '9');
}