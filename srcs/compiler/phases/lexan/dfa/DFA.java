/*
 * See about_license.txt in repo root.
 */
package compiler.phases.lexan.dfa;

import java.util.Iterator;
import java.util.List;

/**
 *  An implementation of constrained  Deterministic Finite Automata (DFA).
 *  
 *  Constrained by the fact that the edge of any cycle has the same vertex for
 *  both of its ends. (For any cycle, only the A->A configuration is allowed)
 *  
 *  This is good enough for regex-like matching.
 */
public class DFA {
    
    // List of states
    private final List<State> states;
    
    // -- stateful variables --
    // If the DFA is in running state, that is:
    //      the DFA is either in start state, or all states
    //      up to the current one had valid transitions,
    //      either through remain or advance matchers
    private boolean running;
    private Iterator<State> stateIterator;
    private State currentState;
    // resets DFA to initial (starting) state
    public final void reset() {
        running = true;
        stateIterator = states.iterator();
        // get the starting state from iterator - has to point to next state
        currentState = stateIterator.next();
    }
    
    public DFA(List<State> states) {
        this.states = states;
        reset();
    }
    
    /**
     * Advances the DFA, if possible.
     * 
     * @param in input char
     * @return if the DFA is still running
     */
    public boolean advance(char in) {
        if(running) {
            if(currentState.canAdvance(in) && stateIterator.hasNext()) {
                currentState = stateIterator.next();
                // if we have achieved final state (by consuming a character that
                // ends the sequence), we have matched - immediately stop DFA
                running = currentState != State.FINAL_STATE;
            }
            else
                running = currentState.canRemain(in);
        }
        return running;
    }
    
    public boolean isRunning() {
        return running;
    }
    
    /**
     * @return if the DFA has terminated and remained in a final state
     */
    public boolean match() {
        return !running && currentState == State.FINAL_STATE;
    }
    
    @Override public String toString() {
        StringBuilder s = new StringBuilder();
        
        s.append(String.format(
                "%7s %7s : ",
                running ? "Running" : "Stopped",
                match() ? "Matched" : ""
        ));
        
        for(State st : states)
            s.append(String.format(st == currentState ? "* %s / " : "%s / ", st.toString()));
        
        return s.toString();
    }
}
