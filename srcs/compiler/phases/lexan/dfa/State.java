/*
 *  See about_license.txt in repo root.
 */
package compiler.phases.lexan.dfa;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Single state of DFA.
 * For proper operation, the remain, advance set of matchers should never
 * match at the same time.
 * (The sets of characters matched by remain, advance matchers should be disjoint)
 */
public class State {
    // character mather lists
    private final List<Matcher> remain, advance;

    public State(
            List<Matcher> remain,
            List<Matcher> advance
    ) {
        this.remain = remain;
        this.advance = advance;
    }
    
    public static final State FINAL_STATE = new State(null, null);
    
    /** Wait state extension.
     *  Since the DFA stops immediately after reaching the final state, we would
     *  have to know if the final state is due to a positive
     *  (matched a character that is part of the lexeme) or negative match, in order
     *  to properly manage on when to push the character back.
     * 
     *  The alternative is to introduce a state that advances regardless of the
     *  character, and for the current character to be always pushed back.
     */
    private static final class WaitState extends State {
        WaitState(){
            super(null, null);
        }
        @Override
        public boolean canAdvance(char in) {
            return true;
        }
        @Override
        public boolean canRemain(char in) {
            return false;
        }
        @Override
        public String toString() {
            return "W";
        }
    }
    public static final State WAIT_STATE = new WaitState();

    /**
     * Returns if we can advance to the next state in the DFA for the
     * specified character.
     * 
     * This happens if no matcher in remain list matched and at least one
     * matcher in advance list did.
     * @param in input char
     * @return if DFA can advance from this state
     */
    public boolean canAdvance(char in) {
        return
                (remain == null || remain.stream().noneMatch(m -> m.match(in))) &&
                (advance != null && advance.stream().anyMatch(m -> m.match(in)));
    }

    /**
     * Returns if we can remain in this state (is repeatable).
     * If we cannot either advance from, nor remain in some state,
     * the DFA is stopped.
     * @param in input char
     * @return if DFA can remain in this state
     */
    public boolean canRemain(char in) {
        return remain != null && remain.stream().anyMatch(m -> m.match(in));
    }
    
    /**
     * Helper method that allows generation of DFAs for keywords.
     * @param keyword that the DFA should match
     * @return DFA
     */
    public static AppendableArrayList<State> getKeywordStates(String keyword) {
        AppendableArrayList<State> states = new AppendableArrayList<>();
        
        for(char curChar : keyword.toCharArray()) {
            
            // add intermediary non-final states
            states.add(
                    new State(null,
                            new AppendableArrayList<Matcher>()
                            .append(new Matcher.CharMatcher(curChar))
                    ));
        }
        // add wait state, since we proceed on positive match
        states.add(WAIT_STATE);
        // add final state that should finish regardless of the character
        // consumed
        states.add(FINAL_STATE);
        
        return states;
    }
    /**
     * Helper method that provides an iterator for given array of states.
     * @return 
     */
    public static Iterator<State> getIterator(final State[] arr) {
        return new Iterator<State>() {
            private State[] arr;
            private int idx = 0;
            
            @Override
            public boolean hasNext() {
                return idx < arr.length;
            }

            @Override
            public State next() {
                if(hasNext())
                    return arr[idx++];
                else
                    throw new IndexOutOfBoundsException();
            }
        };
    }
    
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        
        s.append("R: ");
        if (remain != null)
            s.append(remain.stream().map(Matcher::toString).collect(Collectors.joining(", ")));
        s.append(" A: ");
        if (advance != null)
            s.append(advance.stream().map(Matcher::toString).collect(Collectors.joining(", ")));
        
        return s.toString();
    }
}