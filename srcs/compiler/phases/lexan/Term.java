package compiler.phases.lexan;

import compiler.phases.lexan.dfa.AppendableArrayList;
import compiler.phases.lexan.dfa.Matcher;
import compiler.phases.lexan.dfa.Matcher.UnionMatcher;
import compiler.phases.lexan.dfa.State;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * CFG terminals, i.e., tokens.
 *
 * =======================================================
 * Based on The PREV
 * programming language (academic year 2016/17)
 * =======================================================
 *
 * @author sliva
 *
 */
public enum Term {

    EOF(0, "\uFFFF"), // End Of File reached (matches (char)-1)

    IOR(600, "|"), // |
    XOR(600, "^"), // ^

    AND(600, "&"), // &

    EQU(600, "=="), // ==
    NEQ(600, "!="), // !=
    LTH(550, "<"), // <
    GTH(550, ">"), // >
    LEQ(600, "<="), // <=
    GEQ(600, ">="), // >=

    ADD(600, "+"), // +
    SUB(600, "-"), // -
    MUL(600, "*"), // *
    DIV(600, "/"), // /
    MOD(600, "%"), // %

    NOT(600, "!"), // !

    MEM(600, "$"), // $
    VAL(600, "@"), // @

    NEW(500, "new"), // new
    DEL(500, "del"), // del

    ASSIGN(550, "="), // =

    COLON(600, ":"), // :
    COMMA(600, ","), // ,
    DOT(600, "."), // .
    SEMIC(600, ";"), // ,

    LBRACE(600, "{"), // {
    RBRACE(600, "}"), // }
    LBRACKET(600, "["), // [
    RBRACKET(600, "]"), // ]
    LPARENTHESIS(600, "("), // (
    RPARENTHESIS(600, ")"), // )

    VOIDCONST(500, "none"), // none
    BOOLCONST(500, new String[]{"true", "false"}), // true, false
    CHARCONST(400,
        new AppendableArrayList<State>()
            .append(new State(
                // remain
                null,
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.CharMatcher('\''))
            ))
            .append(new State(
                // remain
                null,
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.CharRangeMatcher((char) 32, (char) 126))
            ))
            .append(new State(
                // remain
                null,
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.CharMatcher('\''))
            ))
            .append(State.WAIT_STATE)
            .append(State.FINAL_STATE)
    ), // '[ascii 32 - ascii 126]'
    INTCONST(400,
        new AppendableArrayList<State>()
            // lead-in
            .append(new State(
                // remain
                null,
                // advance
                new AppendableArrayList<Matcher>()
                    .append(Matcher._09)
            ))
            .append(new State(
                // remain
                new AppendableArrayList<Matcher>()
                    .append(Matcher._09),
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.InverseMatcher(Matcher._09))
            ))
            .append(
                State.FINAL_STATE
            )
    ), // [0 - 9]+
    // Even though the spec states [+-]?[0 - 9]+
    // note, that on the lexical analysis stage, the sign prefix is treated
    // as a separate token
    PTRCONST(500, "null"), // null

    VOID(500, "void"), // void
    BOOL(500, "bool"), // bool
    CHAR(500, "char"), // char
    INT(500, "int"), // int
    PTR(500, "ptr"), // ptr
    ARR(500, "arr"), // arr
    REC(500, "rec"), // rec

    DO(500, "do"), // do
    ELSE(500, "else"), // else
    END(500, "end"), // end
    FUN(500, "fun"), // fun
    IF(500, "if"), // if
    THEN(500, "then"), // then
    TYP(500, "typ"), // typ
    VAR(500, "var"), // var
    WHERE(500, "where"), // where
    WHILE(500, "while"), // while

    IDENTIFIER(100,
        new AppendableArrayList<State>()
            // lead-in
            .append(new State(
                // remain
                null,
                // advance
                new AppendableArrayList<Matcher>()
                    .append(Matcher.az)
                    .append(Matcher.AZ)
                    .append(new Matcher.CharMatcher('_'))
            ))
            .append(new State(
                // remain
                new AppendableArrayList<Matcher>()
                    .append(Matcher.az)
                    .append(Matcher.AZ)
                    .append(new Matcher.CharMatcher('_'))
                    .append(Matcher._09),
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.InverseMatcher(
                        new Matcher.UnionMatcher(
                            new AppendableArrayList<Matcher>()
                                .append(Matcher.az)
                                .append(Matcher.AZ)
                                .append(new Matcher.CharMatcher('_'))
                                .append(Matcher._09)
                        )
                    ))
            ))
            .append(State.FINAL_STATE)
    ), // [_a-zA-Z]+[_a-zA-Z0-9]* + not a keyword
    WHITESPACE(50,
        new AppendableArrayList<State>()
            // lead-in
            .append(new State(
                // remain
                null,
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.CharMatcher(' '))
                    .append(new Matcher.CharMatcher('\t'))
                    .append(new Matcher.CharMatcher('\n'))
            ))
            .append(new State(
                // remain
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.CharMatcher(' '))
                    .append(new Matcher.CharMatcher('\t'))
                    .append(new Matcher.CharMatcher('\n')),
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.InverseMatcher(
                        new Matcher.UnionMatcher(
                            new AppendableArrayList<Matcher>()
                                .append(new Matcher.CharMatcher(' '))
                                .append(new Matcher.CharMatcher('\t'))
                                .append(new Matcher.CharMatcher('\n'))
                        )
                    ))
            ))
            .append(State.FINAL_STATE)
    ),
    COMMENT(1000,
        new AppendableArrayList<State>()
            // lead-in
            .append(new State(
                // remain
                null,
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.CharMatcher('#'))
            ))
            .append(new State(
                // remain
                new AppendableArrayList<Matcher>()
                    .append(
                        new Matcher.InverseMatcher(
                            new UnionMatcher(
                                new AppendableArrayList<Matcher>()
                                    .append(new Matcher.CharMatcher('\n'))
                                    .append(new Matcher.CharMatcher('\uFFFF')))
                        )),
                // advance
                new AppendableArrayList<Matcher>()
                    .append(new Matcher.CharMatcher('\n'))
                    .append(new Matcher.CharMatcher('\uFFFF'))
            ))
            .append(State.WAIT_STATE)
            .append(State.FINAL_STATE)
    );
    /**
     * Specialization of list.
     * 
     * Needed so that we can do list of lists, otherwise we get into issues
     * with diamond operator deletion.
     */
    public static class StateList extends AppendableArrayList<AppendableArrayList<State>> {
    }
    
    /**
     * DFA states. Since multiple keywords can resolve to the same token, we
     * have to support multiple states (and thereby multiple DFAs).
     */
    public final StateList states;
    /**
     * Term priority. Higher value = higher priority.
     */
    public final int priority;

    // constructors
    Term(int priority) {
        this.states = null;
        this.priority = priority;
    }

    Term(int priority, final StateList states) {
        this.priority = priority;
        this.states = states;
    }

    Term(int priority, final AppendableArrayList<State> states) {
        StateList sl = new StateList();
        sl.add(states);
        this.priority = priority;
        this.states = sl;
    }

    Term(int priority, String keywordStates) {
        this(priority, State.getKeywordStates(keywordStates));
    }

    Term(int priority, String[] keywordStates) {
        StateList sl = new StateList();

        for (String keywordState : keywordStates) {
            sl.add(State.getKeywordStates(keywordState));
        }

        this.priority = priority;
        this.states = sl;
    }

    /**
     * Priority sorted list of terms. Sorted by _ascending_ priority value, in
     * order to provide rule priority during parsing. Last longest match is
     * taken as the token - in
     */
    public static final List<Term> prioritised;

    static {
        Term[] sortedTerms = Term.values();
        Arrays.sort(sortedTerms, (a, b) -> a.priority - b.priority);
        prioritised = Collections.unmodifiableList(Arrays.asList(sortedTerms));
    }
}
