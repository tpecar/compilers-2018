package compiler.phases.lexan;

import compiler.phases.lexan.dfa.DFA;
import java.io.*;
import common.report.*;
import compiler.phases.*;
import compiler.phases.lexan.dfa.State;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Lexical analysis.
 * 
 * @author sliva
 *
 */
public class LexAn extends Phase {
    
        /**
         * If enabled, provides detailed internal debugging.
         * The debugging information should be printed out with Report.info,
         * format is implementation dependent.
         */
        public static boolean DEBUG = false;

	/** The name of the source file. */
	private final String srcFileName;

	/** The source file reader. */
	private final BufferedReader srcFile;
        
        /** Current file row, column. */
        private int line, column;
        
        /** Current lexeme. */
        private int begLine, begColumn, endLine, endColumn;
        private StringBuilder lexeme;
        
        /**
         * List of DFAs used for term matching.
         */
        private final List <TermDFA> termDFA;

	/**
	 * Constructs a new lexical analysis phase.
	 */
	public LexAn() {
            super("lexan");
            srcFileName = compiler.Main.cmdLineArgValue("--src-file-name");
            try {
                    srcFile = new BufferedReader(new FileReader(srcFileName));
            } catch (IOException ___) {
                    throw new Report.Error("Cannot open source file '" + srcFileName + "'.");
            }
            line = column = 1;
            // fetch first char from the stream
            getChar();

            ArrayList<TermDFA> alTermDFA = new ArrayList<>();
            for(Term cTerm : Term.prioritised) {
                ArrayList<DFA> alDFA = new ArrayList<>();
                for(List<State> cStateList : cTerm.states)
                    alDFA.add(new DFA(cStateList));

               alTermDFA.add(new TermDFA(cTerm, alDFA));
            }
            termDFA = Collections.unmodifiableList(alTermDFA);
	}
        
        private char readChar = Character.MAX_VALUE;
        
        /** Returns a character from the stream.
         */
        private void getChar() {
            try {
                while ((readChar = (char) srcFile.read()) == '\r') {}
            } catch (IOException ___) {
                throw new Report.Error(String.format("IO fail at [%d, %d]", line, column));
            }
        }
        
        /**
         * Tracks line, column.
         *  
         * For the case of CR LF (Windows), the CR will be internally consumed
         * without counter increment and the next character will be read.
         */
        private void popChar() {
            if (readChar == '\n') {
                column = 1;
                line++;
            } else {
                column++;
            }
        }

	/**
	 * The lexer.
	 * 
	 * This method returns the next symbol from the source file. To perform the
	 * lexical analysis of the entire source file, this method must be called
	 * until it returns EOF. This method calls {@link #lexify()}, logs its
	 * result if requested, and returns it.
	 * 
	 * @return The next symbol from the source file.
	 */
	public Symbol lexer() {
                // we call lexify again in case of whitespace or comments
		Symbol symb;
                do {symb = lexify();}
                while(
                    symb.token == Term.WHITESPACE || symb.token == Term.COMMENT
                );
                
		symb.log(logger);
		return symb;
	}

	@Override
	public void close() {
		try {
			srcFile.close();
		} catch (IOException ___) {
			Report.warning("Cannot close source file '" + this.srcFileName + "'.");
		}
		super.close();
	}

	// --- LEXER ---
        
        private class TermDFA {
            private final List<DFA> dfa;    // DFAs used for matching
            private final Term token;
            private boolean firstMatch;
            
            public final void reset() {
                dfa.forEach(DFA::reset);
                firstMatch = false;
            }
            
            public TermDFA(Term token, List<DFA> dfa) {
                this.token = token;
                this.dfa = dfa;
            }
            
            /**
             * Returns if the DFA matched.
             * Returns true only for first match - any further query means that
             * we are searching for a longer match, therefore this
             */
            public boolean match(char in) {
                if(!firstMatch) {
                    for(DFA cDfa: dfa)
                        firstMatch |= (!cDfa.advance(in) && cDfa.match());
                    return firstMatch;
                }
                return false;
            }
            
            public boolean isRunning() {
                boolean dfaRunning = false;
                for(DFA cDfa : dfa)
                    dfaRunning |= cDfa.isRunning();
                return dfaRunning;
            }
            
            /**
             * Returns the symbol matched by the DFA.
             * @return symbol
             */
            public Symbol getSymbol() {
                return new Symbol(
                        token,
                        // due to log, files, if we get EOF, we have to clear
                        // the lexeme (otherwise we get the \uFFFF char)
                        token == Term.EOF ? "" : lexeme.toString(),
                        new Location(begLine, begColumn, endLine, endColumn)
                );
            }
            
            @Override
            public String toString() {
                return dfa.stream().map(DFA::toString).collect(Collectors.joining("\n"));
            }
        }
        
        private String getTermDFAState() {
            StringBuilder s = new StringBuilder();
            for(TermDFA ctDFA : termDFA)
                s.append(String.format("%-10s \n%s\n\n", ctDFA.token, ctDFA.toString()));
            return s.toString();
        }

	/**
	 * Performs the lexical analysis of the source file.
	 * 
	 * This method returns the next symbol from the source file. To perform the
	 * lexical analysis of the entire source file, this method must be called
	 * until it returns EOF.
	 * 
	 * @return The next symbol from the source file or EOF if no symbol is
	 *         available any more.
	 */
	private Symbol lexify() {
            /*
            * Since multiple TermDFAs can match at the same time, we will implement
            * rule priority by accepting the last longest match:
            *      - we iterate through the array in the forward direction
            *        (lowest idx -> highest idx)
            *      - if the TermDFA matches, we add it into the lastMatched,
            *        overriding the previous value
            *      - we iterate over the array until no TermDFA advances
            *        (push back this character)
            *      - we query the lastMatched for symbol
            * 
            * This means that low priority terms (such as identifiers) have to be
            * included in the array first, so that higher priority terms
            * (such as keywords) will be saved to lastAdvanced last.
            */
            // reset TermDFAs
            termDFA.stream().forEach(TermDFA::reset);

            // new lexeme start/stop to current file position
            begLine = endLine = line;
            begColumn = endColumn = column;
            // new lexeme content
            lexeme = new StringBuilder();
            
            boolean dfaRunning = true;
            TermDFA lastMatched = null;
            
            while(dfaRunning) {
                dfaRunning = false;

                for(TermDFA ctDFA : termDFA) {
                    if(ctDFA.match(readChar))
                        lastMatched = ctDFA;
                    
                    dfaRunning |= ctDFA.isRunning();
                }
                if(DEBUG)
                    Report.info(String.format("DFA after [%d = %c]\n%s", (int)readChar, readChar, getTermDFAState()));
                if(dfaRunning) {
                    endLine = line;
                    endColumn = column;
                
                    // current input char allowed the DFA to advance, therefore
                    // it is part of the lexeme
                    lexeme.append(readChar);
                    popChar();
                    getChar();
                }
            }
            if(lastMatched == null)
                throw new Report.Error(new Location(line, column),
                    String.format("[%d %d - %d %d] invalid symbol \"%s\"\n",
                        begLine, begColumn, endLine, endColumn,
                        lexeme.toString()+readChar));
            
            return lastMatched.getSymbol();
	}

}
