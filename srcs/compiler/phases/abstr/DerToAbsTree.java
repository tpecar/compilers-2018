package compiler.phases.abstr;

import common.report.*;
import compiler.phases.abstr.abstree.*;
import compiler.phases.lexan.Term;
import compiler.phases.synan.*;
import compiler.phases.synan.dertree.*;

import static compiler.phases.lexan.Term.*;
import static compiler.phases.synan.Nont.*;
import java.util.Vector;

/**
 * Transforms a derivation tree into an abstract syntax tree.
 * 
 * See compiler.phases.synan.Grammar.txt for details on the structure of the
 * derivation tree we are trying to convert.
 * 
 * @author sliva
 *
 */
public class DerToAbsTree implements DerVisitor<AbsTree, AbsTree> {
    public static final DerToAbsTree DER_TO_ABS_TREE = new DerToAbsTree();
    
    // we will only directly visit DerLeaf nodes when they will present as the
    // only token in the file - any other case will match
    // a nonterminal expansion
    public AbsTree visit(DerLeaf leaf, AbsTree visArg) {
        if(Abstr.DEBUG)
            Report.info(String.format("DerLeaf   : %10s %5s: %s",
                leaf.location(), leaf.symb.token, leaf.symb.lexeme));
        // (literal )
        //   expr -> _literal
        switch(leaf.symb.token) {
            case VOIDCONST: return new AbsAtomExpr(leaf, AbsAtomExpr.Type.VOID, leaf.symb.lexeme);
            case BOOLCONST: return new AbsAtomExpr(leaf, AbsAtomExpr.Type.BOOL, leaf.symb.lexeme);
            case CHARCONST: return new AbsAtomExpr(leaf, AbsAtomExpr.Type.CHAR, leaf.symb.lexeme);
            case INTCONST:  return new AbsAtomExpr(leaf, AbsAtomExpr.Type.INT, leaf.symb.lexeme);
            case PTRCONST:  return new AbsAtomExpr(leaf, AbsAtomExpr.Type.PTR, leaf.symb.lexeme);
            // __type__ atoms (like the void type) have to be parsed within nonterminal context
            
            // nodes that have no explicit conversion to AST nodes have to be
            // picked up during nonterminal conversion
            default:
                throw new Report.Error(String.format("Encountered terminal %s (%s) without context, can not convert to AST.", leaf.symb.lexeme, leaf.symb.token));
        }
    }
    
    /**
     * Descendable.
     * Used for going through the derivation tree.
     */
    private interface Descendable {
        public boolean m(Term t);
        public boolean m(Nont n);
        public Descendable d(int index);
        public AbsTree accept(AbsTree visArg);
        public DN dn();
        public DL dl();
        @Override
        public String toString();
    }
    /**
     * Descendable DerNode - Provides a descend operation for DerNode.
     * If the descend is not possible (no child at specified index), we provide
     * a DStop implementation that silently ignores the descent request.
     * 
     * In case the subtree is a leaf, we return it as a DL.
     * 
     * When returning a descendable, objects have to be copied in order to
     * allow for type conversion from unrelated specialized type.
     * 
     * Do note that type conversion has to be done for each child on each
     * depth.
     */
    private static class DN extends DerNode implements Descendable {
        public DN(DerNode dn) {
            super(dn);
        }
        @Override
        public Descendable d(int index) {
            if(index < subtrees.size()) {
                // type conversion through copy constructor
                if(subtrees.elementAt(index) instanceof DerLeaf)
                    return new DL((DerLeaf)subtrees.elementAt(index));
                if(subtrees.elementAt(index) instanceof DerNode)
                    return new DN((DerNode)subtrees.elementAt(index));
                if(subtrees.elementAt(index) instanceof DerEpsilon)
                    return new DE((DerEpsilon)subtrees.elementAt(index));
            }
            // could not descend
            return DSTOP;
        }
        /**
         * Descends into this node for the specified depth, always taking the
         * first subtree.
         * @param depth
         * @return 
         */
        public Descendable dFirst(int depth) {
            Descendable c = this;
            for(int i=0; i<depth; i++)
                c = c.d(0);
            return c;
        }
        @Override
        public boolean m(Term t) {
            // DerNode contains nonterminals, never matches a terminal
            return false;
        }
        @Override
        public boolean m(Nont n) {
            return n == this.label;
        }
        @Override
        public AbsTree accept(AbsTree visArg) {
            return super.accept(DER_TO_ABS_TREE, visArg);
        }
        @Override
        public DN dn(){
            return this;
        }
        @Override
        public DL dl(){
            throw new Report.InternalError();
        }
        @Override
        public String toString(){
            return String.format("N %s", this.label.name());
        }
    }
    private static DN DN(DerNode dn) {
        // type conversion through copy constructor
        return new DN(dn);
    }
    private static class DL extends DerLeaf implements Descendable {
        public DL(DerLeaf dl) {
            super(dl);
        }
        @Override
        public Descendable d(int index) {
            // since we are in a leaf, we cannot descent further
            return DSTOP;
        }
        @Override
        public boolean m(Term t) {
            return t == this.symb.token;
        }
        @Override
        public boolean m(Nont n) {
            return false;
        }
        @Override
        public AbsTree accept(AbsTree visArg) {
            return super.accept(DER_TO_ABS_TREE, visArg);
        }
        @Override
        public DN dn() {
            throw new Report.InternalError();
        }
        @Override
        public DL dl() {
            return this;
        }
        @Override
        public String toString() {
            return String.format("T %s (%s)", this.symb.lexeme, this.symb.token);
        }
    }
    private static DL DL(DerLeaf dl) {
        return (DL)dl;
    }
    private static class DE extends DerEpsilon implements Descendable {
        public DE(DerEpsilon de) {
            super(de);
        }
        @Override
        public Descendable d(int index) {
            return this;
        }
        @Override
        public boolean m(Term t) {
            return false;
        }
        @Override
        public boolean m(Nont n) {
            return false;
        }
        @Override
        public AbsTree accept(AbsTree visArg) {
             return super.accept(DER_TO_ABS_TREE, visArg);
        }
        @Override
        public DN dn() {
            throw new Report.InternalError();
        }
        @Override
        public DL dl() {
            throw new Report.InternalError();
        }
        @Override
        public String toString() {
            return "Eps";
        }
    }
    private static class DStop implements Descendable {
        @Override
        public Descendable d(int index) {
            return this;
        }
        @Override
        public boolean m(Term t) {
            return false;
        }
        @Override
        public boolean m(Nont n) {
            return false;
        }
        @Override
        public AbsTree accept(AbsTree visArg) {
            return null;
        }
        @Override
        public DN dn() {
            throw new Report.InternalError();
        }
        @Override
        public DL dl() {
            throw new Report.InternalError();
        }
        @Override
        public String toString() {
            return "X";
        }
    }
    // a static instance of DStop
    private final static DStop DSTOP = new DStop();
    
    public AbsTree visit(DerNode node, AbsTree visArg) {
        if(Abstr.DEBUG)
            Report.info(String.format("DerNode   : %10s %-5s",
                node.location(), node.label));
        
        DN dn = DN(node);
        
        // Since we are recursively calling the accept -> visit methods,
        // we should get to a stage where the visitor will return a
        // Abs* node
        // However, in order for this to work correctly, we have to go through
        // all nonterminals.
        
        switch(node.label) {
            case Source:    return dn.d(0).accept(visArg);

            case expr:      return dn.d(0).accept(visArg);

                            
            case orExpr:
                return  // andExpr orExprX
                    dn.d(1).accept(                // R subtree
                        dn.d(0).accept(visArg)     // L subtree
                    );

            case orExprX:
                // handles left subtree
                if(dn.d(0).m(IOR)) // _IOR andExpr orExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.IOR, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                if(dn.d(0).m(XOR)) // _XOR andExpr orExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.XOR, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                break;
            
            case andExpr:
                return // compExpr andExprX
                    dn.d(1).accept(
                        dn.d(0).accept(visArg)
                    );

            case andExprX:
                // handles left subtree
                if(dn.d(0).m(AND)) // _AND compExpr andExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.AND, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                break;

            case compExpr:
                return // addExpr compExprX
                    dn.d(1).accept(
                        dn.d(0).accept(visArg)
                    );

            case compExprX:
                // handles left subtree
                if(dn.d(0).m(EQU)) // _EQU addExpr
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.EQU, (AbsExpr)visArg,
                            (AbsExpr)dn.d(1).accept(null)
                        );
                if(dn.d(0).m(NEQ)) // _NEQ addExpr
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.NEQ, (AbsExpr)visArg,
                            (AbsExpr)dn.d(1).accept(null)
                        );
                if(dn.d(0).m(LEQ)) // _LEQ addExpr
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.LEQ, (AbsExpr)visArg,
                            (AbsExpr)dn.d(1).accept(null)
                        );
                if(dn.d(0).m(GEQ)) // _GEQ addExpr
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.GEQ, (AbsExpr)visArg,
                            (AbsExpr)dn.d(1).accept(null)
                        );
                if(dn.d(0).m(LTH)) // _LTH addExpr
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.LTH, (AbsExpr)visArg,
                            (AbsExpr)dn.d(1).accept(null)
                        );
                if(dn.d(0).m(GTH)) // _GTH addExpr
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.GTH, (AbsExpr)visArg,
                            (AbsExpr)dn.d(1).accept(null)
                        );
                break;
                
            case addExpr:
                return // mulExpr addExprX
                    dn.d(1).accept(
                        dn.d(0).accept(visArg)
                    );
                
            case addExprX:
                // possibly missing termination handling
                //  (check how the null nonterminal visitor gets handled)
                // handles left subtree
                if(dn.d(0).m(ADD)) // _ADD mulExpr addExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.ADD, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                if(dn.d(0).m(SUB)) // _SUB mulExpr addExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.SUB, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                break;
            
            case mulExpr:
                return // unaryExpr mulExprX
                    dn.d(1).accept(
                        dn.d(0).accept(visArg)
                    );

            case mulExprX:
                // handles left subtree
                if(dn.d(0).m(MUL)) // _MUL unaryExpr mulExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.MUL, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                if(dn.d(0).m(DIV)) // _DIV unaryExpr mulExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.DIV, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                if(dn.d(0).m(MOD)) // _MOD unaryExpr mulExprX
                    return
                        new AbsBinExpr(node, AbsBinExpr.Oper.MOD, (AbsExpr)visArg,
                        (AbsExpr)dn.d(2).accept(
                            (AbsExpr)dn.d(1).accept(null)
                        ));
                break;
            
            case unaryExpr:
                if(dn.d(0).m(ADD)) // _ADD unaryExpr
                    return
                        new AbsUnExpr(node, AbsUnExpr.Oper.ADD, (AbsExpr)dn.d(1).accept(null));
                if(dn.d(0).m(SUB)) // _SUB unaryExpr
                    return
                        new AbsUnExpr(node, AbsUnExpr.Oper.SUB, (AbsExpr)dn.d(1).accept(null));
                if(dn.d(0).m(NOT)) // _NOT unaryExpr
                    return
                        new AbsUnExpr(node, AbsUnExpr.Oper.NOT, (AbsExpr)dn.d(1).accept(null));
                if(dn.d(0).m(MEM)) // _MEM unaryExpr
                    return
                        new AbsUnExpr(node, AbsUnExpr.Oper.MEM, (AbsExpr)dn.d(1).accept(null));
                if(dn.d(0).m(NEW)) // _NEW type
                    return
                        new AbsNewExpr(node, (AbsType)dn.d(1).accept(null));
                if(dn.d(0).m(DEL)) // _DEL unaryExpr
                    return
                        new AbsDelExpr(node, (AbsExpr)dn.d(1).accept(null));
                if(dn.d(0).m(ADD)) // _VAL unaryExpr
                    return
                        new AbsUnExpr(node, AbsUnExpr.Oper.VAL, (AbsExpr)dn.d(1).accept(null));
                if(dn.d(0).m(LBRACKET)) // _LBRACKET type _RBRACKET unaryExpr
                    return
                        new AbsCastExpr(node,
                            (AbsType)dn.d(1).accept(null),
                            (AbsExpr)dn.d(3).accept(null)
                        );
                
                // accessExpr
                return
                    dn.d(0).accept(visArg);
                
            case accessExpr:
                return // valueExpr accessExprX
                    dn.d(1).accept(
                        dn.d(0).accept(visArg)
                    );

            case accessExprX:
                // class definition for AbsRecExpr does not allow for
                // multi-level component access
                if(dn.d(0).m(DOT)) // _DOT _IDENTIFIER [accessExprX]
                    return new AbsRecExpr(
                        node,
                        (AbsExpr)visArg,
                        new AbsVarName(dn.d(1).dl(), dn.d(1).dl().symb.lexeme)
                    );
            
                if(dn.d(0).m(LBRACKET)) // _LBRACKET expr _RBRACKET __accessExprX__
                    // currently accessExprX is ignored, check if that is ok
                    return new AbsArrExpr(node, (AbsExpr)visArg, (AbsExpr)dn.d(1).accept(null));
                break;

            case valueExpr:
                if(dn.d(0).m(IDENTIFIER)) { // _IDENTIFIER idFunExpr
                    AbsArgs idFunExpr = (AbsArgs)dn.d(1).accept(null);
                    if(idFunExpr!=null)
                        return new AbsFunName(
                            node,
                            dn.d(0).dl().symb.lexeme,
                            idFunExpr
                        );
                    else
                        return new AbsVarName(dn.d(0).dl(), dn.d(0).dl().symb.lexeme);
                }
                if(dn.d(0).m(LPARENTHESIS)) // _LPARENTHESIS expr _RPARENTHESIS
                    return dn.d(1).accept(null);
                if(dn.d(0).m(LBRACE)) // _LBRACE stmt stmtX _COLON expr whereExpr _RBRACE
                        return new AbsStmtExpr(
                            node,
                            // be aware that arguments are evaluated left-to-right,
                            // so the subtrees might be visited in non-sequential
                            // manner
                            (AbsDecls)dn.d(5).accept(null), // whereExpr
                            (AbsStmts)dn.d(2).accept(       // stmtX
                                dn.d(1).accept(null)        // stmt
                            ), 
                            (AbsExpr)dn.d(4).accept(null)   // expr
                    );
                
                //_VOIDCONST
                //_BOOLCONST
                //_CHARCONST
                //_INTCONST
                //_PTRCONST
                // visit leaf
                return dn.d(0).dl().accept(this, visArg);

            case idFunExpr:
                if(dn.d(0).m(LPARENTHESIS)) // _LPARENTHESIS idFunExprParams _RPARENTHESIS
                    return (AbsArgs)dn.d(1).accept(null);
                break;

            case idFunExprParams: // expr idFunExprParamsX
                return dn.d(1).accept(
                    (AbsExpr)dn.d(0).accept(null)
                );
            
            // we always do a descent to the follow nonterminal - the follow
            // nonterminal tries to descend further, and then uses the
            // previous + follow nodes to assemble the Abs* object
            case idFunExprParamsX: { // _COMMA expr idFunExprParamsX
                Vector<AbsExpr> absArgs = new Vector<>();
                
                // add left hand side
                if(visArg instanceof AbsExpr)
                    absArgs.add((AbsExpr)visArg);
                if(visArg instanceof AbsArgs)
                    absArgs.addAll(((AbsArgs)visArg).args());
                
                if(dn.d(0).m(COMMA)) {
                    absArgs.add((AbsExpr)dn.d(1).accept(null));
                    // assembles the AST parent node AbsArgs with all
                    // previous children + current child (individual AbsArg)
                    
                    // recursively descend into the next parameter,
                    // the provided AST node acts as a wrapper that allows us
                    // carry along previous + current parameters
                    //  (it will get repackaged in the recursive call)
                    return dn.d(2).accept(new AbsArgs(node, absArgs));
                }
                // no next parameter, finish argument descend by providing the
                // last assembled AST node
                return new AbsArgs(node, absArgs);
            }

            case stmtX: { // _SEMIC stmt stmtX
                Vector<AbsStmt> absStmts = new Vector<>();
                
                if(visArg instanceof AbsStmt)
                    absStmts.add((AbsStmt)visArg);
                if(visArg instanceof AbsStmts)
                    absStmts.addAll(((AbsStmts)visArg).stmts());
                
                if(dn.d(0).m(SEMIC)) {
                    absStmts.add((AbsStmt)dn.d(1).accept(null));
                    return dn.d(2).accept(new AbsStmts(node, absStmts));
                }
                return new AbsStmts(node, absStmts);
            }

            case whereExpr: // _WHERE decl declX
                if(dn.d(0).m(WHERE))
                    return dn.d(2).accept(
                        dn.d(1).accept(null)
                    );
                break;

            case declX: { // _SEMIC decl declX
                Vector<AbsDecl> absDecls = new Vector<>();
                
                if(visArg instanceof AbsDecl)
                    absDecls.add((AbsDecl)visArg);
                if(visArg instanceof AbsDecls)
                    absDecls.addAll(((AbsDecls)visArg).decls());
                
                if(dn.d(0).m(SEMIC)) {
                    absDecls.add((AbsDecl)dn.d(1).accept(null));
                    return dn.d(2).accept(new AbsDecls(dn, absDecls));
                }
                return new AbsDecls(dn, absDecls);
            }

            case stmt:
                if(dn.d(0).m(IF)) // _IF expr _THEN stmt stmtX elseStmt _END
                    return new AbsIfStmt(
                        node,
                        (AbsExpr)dn.d(1).accept(null),
                        (AbsStmts)dn.d(4).accept(
                            dn.d(3).accept(null)
                        ),
                        (AbsStmts)dn.d(5).accept(null)
                    );
                if(dn.d(0).m(WHILE)) // _WHILE expr _DO stmt stmtX _END
                    return new AbsWhileStmt(
                        node,
                        (AbsExpr)dn.d(1).accept(null),
                        (AbsStmts)dn.d(4).accept(
                            dn.d(3).accept(null)
                        )
                    );
                // expr assignStmt
                return 
                    dn.d(1).accept(            // assignStmt
                        (AbsExpr)dn.d(0).accept(null)   // expr
                    );

            case assignStmt: // _ASSIGN expr
                // handles left subtree
                if(dn.d(0).m(ASSIGN))
                    return new AbsAssignStmt(
                        node,
                        (AbsExpr)visArg,
                        (AbsExpr)dn.d(1).accept(null)
                    );
                // if we don't have the ASSIGN token, then we don't have a
                // follow expression - we treat the left hand side as a
                // self-standing expression statement
                return new AbsExprStmt(node, (AbsExpr)visArg);

            case elseStmt: // _ELSE stmt stmtX.
                if(dn.d(0).m(ELSE))
                    return dn.d(2).accept(
                        dn.d(1).accept(null)
                    );
                break;

            case type:
                if(dn.d(0).m(VOID)) // _VOID
                    return new AbsAtomType(node, AbsAtomType.Type.VOID);
                if(dn.d(0).m(BOOL)) // _BOOL
                    return new AbsAtomType(node, AbsAtomType.Type.BOOL);
                if(dn.d(0).m(CHAR)) // _CHAR
                    return new AbsAtomType(node, AbsAtomType.Type.CHAR);
                if(dn.d(0).m(INT)) // _INT
                    return new AbsAtomType(node, AbsAtomType.Type.INT);
                if(dn.d(0).m(ARR)) // _ARR _LBRACKET expr _RBRACKET type
                    return new AbsArrType(
                        node,
                        (AbsExpr)dn.d(2).accept(null),
                        (AbsType)dn.d(4).accept(null)
                    );
                if(dn.d(0).m(PTR)) // _PTR type
                    return new AbsPtrType(
                        node,
                        (AbsType)dn.d(1).accept(null)
                    );
                if(dn.d(0).m(REC)) // _REC _LPARENTHESIS declRecParams _RPARENTHESIS
                    return new AbsRecType(
                        node,
                        (AbsCompDecls)dn.d(2).accept(null)
                    );
                // _IDENTIFIER
                return new AbsTypeName(node, dn.d(0).dl().symb.lexeme);

            case declRecParams:
                if(dn.d(0).m(IDENTIFIER)) // _IDENTIFIER _COLON type declRecParamsX
                    // supposedly, records are called "composites" in the AST
                    // we call the declRecParamsX to generate the list for us
                    return dn.d(3).accept(
                        new AbsCompDecl(
                            node,
                            dn.d(0).dl().symb.lexeme,
                            (AbsType)dn.d(2).accept(null)
                        ));
                // if no record elements
                return new AbsCompDecls(dn, new Vector<>());

            case declRecParamsX: { // _COMMA _IDENTIFIER _COLON type declRecParamsX
                Vector<AbsCompDecl> absCompDecls = new Vector<>();
                
                if(visArg instanceof AbsCompDecl)
                    absCompDecls.add((AbsCompDecl)visArg);
                if(visArg instanceof AbsCompDecls)
                    absCompDecls.addAll(((AbsCompDecls)visArg).compDecls());
                
                if(dn.d(0).m(COMMA)) {
                    absCompDecls.add(
                        new AbsCompDecl(
                            node,
                            dn.d(1).dl().symb.lexeme,
                            (AbsType)dn.d(3).accept(null)
                        ));
                    return dn.d(4).accept(new AbsCompDecls(node, absCompDecls));
                }
                return new AbsCompDecls(node, absCompDecls);
            }
            
            case decl:
                if(dn.d(0).m(TYP)) // _TYP _IDENTIFIER _COLON type
                    return new AbsTypeDecl(
                        node,
                        dn.d(1).dl().symb.lexeme,
                        (AbsType)dn.d(3).accept(null)
                    );
                if(dn.d(0).m(VAR)) // _VAR _IDENTIFIER _COLON type
                    return new AbsVarDecl(
                        node,
                        dn.d(1).dl().symb.lexeme,
                        (AbsType)dn.d(3).accept(null)
                    );
                if(dn.d(0).m(FUN)) { // _FUN _IDENTIFIER _LPARENTHESIS declFunParams _RPARENTHESIS _COLON type declExtra
                    AbsExpr funBody = (AbsExpr)dn.d(7).accept(null);
                    
                    if(funBody == null)
                        return
                            new AbsFunDecl(
                                node,
                                dn.d(1).dl().symb.lexeme,
                                (AbsParDecls)dn.d(3).accept(null),
                                (AbsType)dn.d(6).accept(null)
                            );
                    else
                        return
                            new AbsFunDef(
                                node,
                                dn.d(1).dl().symb.lexeme,
                                (AbsParDecls)dn.d(3).accept(null),
                                (AbsType)dn.d(6).accept(null),
                                funBody
                            );
                }
                break;

            case declFunParams:
                if(dn.d(0).m(IDENTIFIER)) // _IDENTIFIER _COLON type declFunParamsX
                    return dn.d(3).accept(
                        new AbsParDecl(
                            node,
                            dn.d(0).dl().symb.lexeme,
                            (AbsType)dn.d(2).accept(null)
                        )
                    );
                // if no parameters
                return new AbsParDecls(dn, new Vector<>());

            case declFunParamsX: { // _COMMA _IDENTIFIER _COLON type declFunParamsX
                Vector<AbsParDecl> absParDecls = new Vector<>();
                
                if(visArg instanceof AbsParDecl)
                    absParDecls.add((AbsParDecl)visArg);
                if(visArg instanceof AbsParDecls)
                    absParDecls.addAll(((AbsParDecls)visArg).parDecls());
                
                if(dn.d(0).m(COMMA)) {
                    absParDecls.add(new AbsParDecl(
                        node,
                        dn.d(1).dl().symb.lexeme,
                        (AbsType)dn.d(3).accept(null)
                    ));
                    return dn.d(4).accept(new AbsParDecls(node, absParDecls));
                }
                return new AbsParDecls(node, absParDecls);
            }

            case declExtra: // _ASSIGN expr
                if(dn.d(0).m(ASSIGN))
                    return dn.d(1).accept(null);
                break;
            
            default:
                throw new Report.Error(String.format("DerNode (%s) has no AST conversion", node.label));
        }
        if(Abstr.DEBUG)
            Report.info("   no match descend");
        // We get here if the current derivation is nullable - in this case, we
        // recursively descend to
        //  - a DerEpsilon node, which returns back the currently assembled
        //    left subtree (visArg)
        //  - a series of other deeper nodes until a leaf is reached, which
        //    normally throws an error
        //    This is an error case, which arises because the SynAn allowed a
        //    structure of nodes that DerToAbsTree couldn't handle.
        //    TODO: provide a cleaner error handling
        return dn.d(0).accept(visArg);
    }
    
    /**
     * Epsilon visitor.
     * 
     * @param node the visited epsilon node
     * @param visArg the left subtree
     * @return the left subtree (no changes)
     */
    @Override
    public AbsTree visit(DerEpsilon node, AbsTree visArg) {
        if(Abstr.DEBUG)
            Report.info(String.format("DerEpsilon: %10s", node.location()));
        return visArg;
    }
}
