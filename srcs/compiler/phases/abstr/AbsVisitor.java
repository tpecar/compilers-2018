package compiler.phases.abstr;

import common.report.*;
import compiler.phases.abstr.abstree.*;

/**
 * An abstract visitor of the abstract syntax tree.
 * 
 * @author sliva
 *
 * @param <Result>
 *            The result the visitor produces.
 * @param <Arg>
 *            The argument the visitor carries around.
 */
public interface AbsVisitor<Result, Arg> {
    
        /** Default method common operations. */
        static class DefaultOps {
            static void illegalVisit(Object o) {
                throw new Report.InternalError(
                    String.format(
                        "Unhandled visitor for %s, will now fail",
                        o.getClass().getName()
                    )
                );
            }
        }

	public default Result visit(AbsArgs args, Arg visArg) {
		DefaultOps.illegalVisit(args);
                return null;
	}

	public default Result visit(AbsArrExpr arrExpr, Arg visArg) {
		DefaultOps.illegalVisit(arrExpr);
                return null;
	}

	public default Result visit(AbsArrType arrType, Arg visArg) {
		DefaultOps.illegalVisit(arrType);
                return null;
	}

	public default Result visit(AbsAssignStmt assignStmt, Arg visArg) {
		DefaultOps.illegalVisit(assignStmt);
                return null;
	}

	public default Result visit(AbsAtomExpr atomExpr, Arg visArg) {
		DefaultOps.illegalVisit(atomExpr);
                return null;
	}

	public default Result visit(AbsAtomType atomType, Arg visArg) {
		DefaultOps.illegalVisit(atomType);
                return null;
	}

	public default Result visit(AbsBinExpr binExpr, Arg visArg) {
		DefaultOps.illegalVisit(binExpr);
                return null;
	}

	public default Result visit(AbsCastExpr castExpr, Arg visArg) {
		DefaultOps.illegalVisit(castExpr);
                return null;
	}

	public default Result visit(AbsCompDecl compDecl, Arg visArg) {
		DefaultOps.illegalVisit(compDecl);
                return null;
	}

	public default Result visit(AbsCompDecls compDecls, Arg visArg) {
		DefaultOps.illegalVisit(compDecls);
                return null;
	}

	public default Result visit(AbsDecls decls, Arg visArg) {
		DefaultOps.illegalVisit(decls);
                return null;
	}

	public default Result visit(AbsDelExpr delExpr, Arg visArg) {
		DefaultOps.illegalVisit(delExpr);
                return null;
	}

	public default Result visit(AbsExprStmt exprStmt, Arg visArg) {
		DefaultOps.illegalVisit(exprStmt);
                return null;
	}

	public default Result visit(AbsFunDecl funDecl, Arg visArg) {
		DefaultOps.illegalVisit(funDecl);
                return null;
	}

	public default Result visit(AbsFunDef funDef, Arg visArg) {
		DefaultOps.illegalVisit(funDef);
                return null;
	}

	public default Result visit(AbsFunName funName, Arg visArg) {
		DefaultOps.illegalVisit(funName);
                return null;
	}

	public default Result visit(AbsIfStmt ifStmt, Arg visArg) {
		DefaultOps.illegalVisit(ifStmt);
                return null;
	}

	public default Result visit(AbsNewExpr newExpr, Arg visArg) {
		DefaultOps.illegalVisit(newExpr);
                return null;
	}

	public default Result visit(AbsParDecl parDecl, Arg visArg) {
		DefaultOps.illegalVisit(parDecl);
                return null;
	}

	public default Result visit(AbsParDecls absPars, Arg visArg) {
		DefaultOps.illegalVisit(absPars);
                return null;
	}

	public default Result visit(AbsPtrType ptrType, Arg visArg) {
		DefaultOps.illegalVisit(ptrType);
                return null;
	}

	public default Result visit(AbsRecExpr recExpr, Arg visArg) {
		DefaultOps.illegalVisit(recExpr);
                return null;
	}

	public default Result visit(AbsRecType recType, Arg visArg) {
		DefaultOps.illegalVisit(recType);
                return null;
	}

	public default Result visit(AbsStmtExpr stmtExpr, Arg visArg) {
		DefaultOps.illegalVisit(stmtExpr);
                return null;
	}

	public default Result visit(AbsStmts stmts, Arg visArg) {
		DefaultOps.illegalVisit(stmts);
                return null;
	}

	public default Result visit(AbsTypeDecl typeDecl, Arg visArg) {
		DefaultOps.illegalVisit(typeDecl);
                return null;
	}

	public default Result visit(AbsTypeName typeName, Arg visArg) {
		DefaultOps.illegalVisit(typeName);
                return null;
	}

	public default Result visit(AbsUnExpr unExpr, Arg visArg) {
		DefaultOps.illegalVisit(unExpr);
                return null;
	}

	public default Result visit(AbsVarDecl varDecl, Arg visArg) {
		DefaultOps.illegalVisit(varDecl);
                return null;
	}

	public default Result visit(AbsVarName varName, Arg visArg) {
		DefaultOps.illegalVisit(varName);
                return null;
	}

	public default Result visit(AbsWhileStmt whileStmt, Arg visArg) {
		DefaultOps.illegalVisit(whileStmt);
                return null;
	};
}
