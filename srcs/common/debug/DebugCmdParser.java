/*
 *  See about_license.txt in repo root.
 */
package common.debug;

import common.report.Report;
import compiler.phases.abstr.Abstr;
import compiler.phases.lexan.LexAn;
import compiler.phases.seman.SemAn;
import compiler.phases.synan.SynAn;
import java.util.ArrayList;

/**
 * Sets class DEBUG variable based on args input.
 * @author tpecar
 */
public class DebugCmdParser {
    /**
     * Parses args, sets DEBUG
     * @param args
     * @return args without debug parameters (so that the template's parameter
     * parsing code doesn't complain about unidentified arguments
     */
    public static String[] parseArgs(String[] args) {
        ArrayList<String> newArgs = new ArrayList<>();
        for(String arg : args) {
            if (arg.matches("--debug-phase=[^ ]*")) {
                arg = arg.replaceFirst("^[^=]*=", "").toLowerCase();
                
                Report.info(String.format("### Will enable debug for \"%s\" ###", arg));
                
                switch(arg) {
                    case "lexan" : LexAn.DEBUG = true; break;
                    case "synan" : SynAn.DEBUG = true; break;
                    case "abstr" : Abstr.DEBUG = true; break;
                    case "seman" : SemAn.DEBUG = true; break;
                    
                    default : Report.info(String.format("WARN: Debug option for phase \"%s\" not implemented!", arg));
                }
            }
            else
                // pass the argument through
                newArgs.add(arg);
        }
        
        return newArgs.toArray(new String[]{});
    }
}
